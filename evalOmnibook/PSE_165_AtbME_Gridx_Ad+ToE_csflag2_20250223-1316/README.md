



command for evaluation



```python
eval_Omnibook(tons=165, costtype1=ps.pse.CTYPE_ACCIDtb, tonal1=False, octave1=False, det1=False, global1=100, grid=ps.pse.Grid_Exhaustive, costtype2=ps.pse.CTYPE_ADplus, tonal2=True, octave2=True, det2=False, dflag=True, mflag=True, csflag=2)
```



`FRfYc` 

- est. Db maj. (-5)

- 29 diff. dont

  - 21 Cb ou Fb

  - 1 double diese

- [ ] revoir le tie-breaking de `CostAT`
- [ ] option no Cb ? cf. stat Omnibook



mes. 10 (9 dans tables):  (accidents, color, flat, double, harm)

- table 1:    `3:0:3:0:0` sur ligne Db maj
- [ ] bug sur CostA ?? accid. devrait être 1
- [ ] avec B (bécarre), on a aussi accid=1 → devrait être favorisé par Tie Breaker cflat
- table 2:  `3+1:0:0:0:1`
- [ ] idem bug surr accids







avec

```python
eval_Omnibookitem(name='FRfYc', tons=165, costtype1=ps.pse.CTYPE_ACCID, tonal1=False, octave1=False, det1=False, global1=100, grid=ps.pse.Grid_Exhaustive, costtype2=ps.pse.CTYPE_ADplus, tonal2=True, octave2=True, det2=False, dflag=True, mflag=True, csflag=2)
```

i.e. `ACCID` au lieu de `ACCIDtb` pour table 1

```
spelling FRfYc
FRfYc  PSE: spelling with PSE165_AME_Gridx_Ad+ToE
pass 1: 690 notes, 129 bars,
pass 2: 428 notes, 129 bars,
PSE: comp. Table 1 cost1: CostType.CTYPE_ACCID tonal1: False det1: False [debug] BOO
1910.42 ms
PSE: comp. Grid with algo: GridAlgo.Grid_Exhaustive 2347.80 ms
PSE: comp. Table 2 cost2: CostType.CTYPE_ADplus tonal2: True det2: False 754.53 ms
PSE: 1 global(s) in final selection:
PSE: global selected: D- major ( -5 ) index: 99
global ton: NO: ( D- major was <music21.key.KeySignature of no sharps or flats> ),
PSE: renaming with the spelling computed 99 D- major
PSE: diff before rewriting: 18
PSE: rewrite passing notes
PSE: diff after rewriting: 22
parts spelled   : 1
notes spelled   : 428
correct spelling: 94.86 %
```

sans Cb etc cf. `ENHARMONICb`

```
spelling FRfYc
FRfYc  PSE: spelling with PSE165_AME_Gridx_Ad+ToE
pass 1: 690 notes, 129 bars,
pass 2: 428 notes, 129 bars,
PSE: comp. Table 1 cost1: CostType.CTYPE_ACCID tonal1: False det1: False [debug] BOO
1905.90 ms
PSE: comp. Grid with algo: GridAlgo.Grid_Exhaustive 2348.24 ms
PSE: comp. Table 2 cost2: CostType.CTYPE_ADplus tonal2: True det2: False 758.44 ms
PSE: 1 global(s) in final selection:
PSE: global selected: D- major ( -5 ) index: 99
global ton: NO: ( D- major was <music21.key.KeySignature of no sharps or flats> ),
PSE: renaming with the spelling computed 99 D- major
PSE: diff before rewriting: 18
PSE: rewrite passing notes
PSE: diff after rewriting: 22
parts spelled   : 1
notes spelled   : 428
correct spelling: 94.86 %
correct KS estim: 0.00 %
```



