# PSE_165_AMD_Gridx_AD+TD_20250130-1226



## problèmes récurrents



- sensibles (F#, C#...) de la tonalité locale (de la grille)
  question de causalité:
  - la tonalité locale a été choisie à cause de ces F# etc dans la première table
    i.e. mauvais choix de grille
  - ou ces F# etc sont préférés à cause de la  tonalité locale 
    (Cost dans le calcul de la 2ieme grille)



- [ ] faire une évaluation 1 table 165
  indications sur la première table (cas 1 ci-dessus)



- F#, C# en phrygien local (A ou E )



- Cb, Fb, B#, E# et double bémols souvent exclus dans original
  option de PSE ? 
  en les effaçant de la table des 3 choix (transitions best-path de spelling)



## Remarque

- grille loin des accords
  cf. `yp3wc`



- modes grecs exclus dans choix ton global
  pourraient être inclus pour sélectionner juste une KS









