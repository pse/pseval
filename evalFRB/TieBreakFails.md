# Tie break fails

in the computation of Grid

message: 

> current ton vs current best ton



##  Con Alma 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`



```
[warning] PSGrid: tie break fail 27:G# minor (5#) vs 12:B major (5#)
[warning] PSGrid: tie break fail 17:Bb minor (5b) vs 2:Db major (5b)
[warning] PSGrid: tie break fail 90:Cb mixolydian (7b) vs 60:Cb phrygian (7b)
```

- [ ] signaler dernier



## Confirmation

`PSE_135_2_A_MD_AD+_TD_20250122-1101`



```
[warning] PSGrid: tie break fail 19:C minor (3b) vs 4:Eb major (3b)
[warning] PSGrid: tie break fail 8:G major (1#) vs 6:F major (1b)
[warning] PSGrid: tie break fail 68:G phrygian (1#) vs 21:D minor (1b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 21:D minor (1b) vs 8:G major (1#)
```



## Daahood 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 15:Ab minor (7b) vs 0:Cb major (7b)
[warning] PSGrid: tie break fail 27:G# minor (5#) vs 12:B major (5#)
```

OK



##  Darn that dream 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
```

- [ ] signaler diff couleur



## Deep purple

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 8:G major (1#) vs 6:F major (1b)
[warning] PSGrid: tie break fail 8:G major (1#) vs 6:F major (1b)
[warning] PSGrid: tie break fail 21:D minor (1b) vs 8:G major (1#)
[warning] PSGrid: tie break fail 8:G major (1#) vs 6:F major (1b)
[warning] PSGrid: tie break fail 8:G major (1#) vs 6:F major (1b)
[warning] PSGrid: tie break fail 21:D minor (1b) vs 8:G major (1#)
```

- [ ] signaler Major vs Major avec diff. couleur



## Desafinado



```
[warning] PSGrid: tie break fail 22:A minor (0) vs 7:C major (0)
[warning] PSGrid: tie break fail 22:A minor (0) vs 7:C major (0)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 38:E minor mel (1#) vs 36:D minor mel (1b)
[warning] PSGrid: tie break fail 35:G minor mel (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 35:G minor mel (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 21:D minor (1b) vs 8:G major (1#)
[warning] PSGrid: tie break fail 68:G phrygian (1#) vs 21:D minor (1b)
[warning] PSGrid: tie break fail 22:A minor (0) vs 7:C major (0)
```



## Do you know what it means 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 22:A minor (0) vs 7:C major (0)
```



## Early autumn-C 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 19:C minor (3b) vs 4:Eb major (3b)
[warning] PSGrid: tie break fail 19:C minor (3b) vs 4:Eb major (3b)
```



## Easy to love 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 22:A minor (0) vs 7:C major (0)
```





## Epistrophy

`PSE_135_2_A_MD_AD+_TD_20250122-1101`

```
[warning] PSGrid: tie break fail 17:Bb minor (5b) vs 2:Db major (5b)
[warning] PSGrid: tie break fail 90:Cb mixolydian (7b) vs 60:Cb phrygian (7b)
```

le dernier est un vrai fail



##  Five brothers 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`



```
[warning] PSGrid: tie break fail 19:C minor (3b) vs 10:A major (3#)
[warning] PSGrid: tie break fail 68:G phrygian (1#) vs 21:D minor (1b)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 9:D major (2#)
```



##  Freight trane 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`



```
[warning] PSGrid: tie break fail 40:F# minor mel (3#) vs 19:C minor (3b)
[warning] PSGrid: tie break fail 15:Ab minor (7b) vs 0:Cb major (7b)
[warning] PSGrid: tie break fail 27:G# minor (5#) vs 12:B major (5#)
[warning] PSGrid: tie break fail 27:G# minor (5#) vs 12:B major (5#)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 25:F# minor (3#) vs 10:A major (3#)
[warning] PSGrid: tie break fail 20:G minor (2b) vs 5:Bb major (2b)
[warning] PSGrid: tie break fail 24:B minor (2#) vs 9:D major (2#)
```





## Golden lady 

`PSE_135_2_A_MD_AD+_TD_20250122-1101`



```
[warning] PSGrid: tie break fail 21:D minor (1b) vs 6:F major (1b)
[warning] PSGrid: tie break fail 21:D minor (1b) vs 8:G major (1#)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
[warning] PSGrid: tie break fail 99:D mixolydian (2#) vs 20:G minor (2b)
```

