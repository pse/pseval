<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE score-partwise  PUBLIC "-//Recordare//DTD MusicXML 4.0 Partwise//EN" "http://www.musicxml.org/dtds/partwise.dtd">
<score-partwise version="4.0">
  <work>
    <work-title>Little waltz</work-title>
  </work>
  <movement-title>Little waltz.musicxml</movement-title>
  <identification>
    <creator type="composer">Music21</creator>
    <rights>cc-by 4.0</rights>
    <encoding>
      <encoding-date>2025-01-30</encoding-date>
      <software>music21 v.9.1.0</software>
      <software>MuseScore 4.4.4</software>
      <supports attribute="new-system" element="print" type="yes" value="yes" />
      <supports attribute="new-page" element="print" type="yes" value="yes" />
    </encoding>
  </identification>
  <defaults>
    <scaling>
      <millimeters>6.99911</millimeters>
      <tenths>40</tenths>
    </scaling>
    <page-layout>
      <page-height>1696.94</page-height>
      <page-width>1200.48</page-width>
      <page-margins>
        <left-margin>85.7252</left-margin>
        <right-margin>85.7252</right-margin>
        <top-margin>85.7252</top-margin>
        <bottom-margin>85.7252</bottom-margin>
      </page-margins>
    </page-layout>
    <appearance>
      <line-width type="light barline">1.8</line-width>
      <line-width type="heavy barline">5.5</line-width>
      <line-width type="beam">5</line-width>
      <line-width type="bracket">4.5</line-width>
      <line-width type="dashes">1</line-width>
      <line-width type="enclosure">1</line-width>
      <line-width type="ending">1.1</line-width>
      <line-width type="extend">1</line-width>
      <line-width type="leger">1.6</line-width>
      <line-width type="pedal">1.1</line-width>
      <line-width type="octave shift">1.1</line-width>
      <line-width type="slur middle">2.1</line-width>
      <line-width type="slur tip">0.5</line-width>
      <line-width type="staff">1.1</line-width>
      <line-width type="stem">1</line-width>
      <line-width type="tie middle">2.1</line-width>
      <line-width type="tie tip">0.5</line-width>
      <line-width type="tuplet bracket">1</line-width>
      <line-width type="wedge">1.2</line-width>
      <note-size type="cue">70</note-size>
      <note-size type="grace">70</note-size>
      <note-size type="grace-cue">49</note-size>
    </appearance>
    <music-font font-family="Leland" />
    <word-font font-family="Edwin" font-size="10" />
    <lyric-font font-family="Edwin" font-size="10" />
  </defaults>
  <credit page="1">
    <credit-words default-x="600.241935" default-y="1611.210312" font-size="22" justify="center" valign="top">Little waltz</credit-words>
  </credit>
  <credit page="1">
    <credit-words default-x="1114.7587" default-y="1511.210312" justify="right" valign="bottom">Ron Carter</credit-words>
  </credit>
  <credit page="1">
    <credit-words default-x="600.241935" default-y="85.725171" font-size="9" justify="center" valign="bottom">cc-by 4.0</credit-words>
  </credit>
  <part-list>
    <score-part id="P1">
      <part-name>Piano</part-name>
      <part-abbreviation>Pno.</part-abbreviation>
      <score-instrument id="Ibc39bf889c818704b55dea82a4df1357">
        <instrument-name>Piano</instrument-name>
        <instrument-abbreviation>Pno</instrument-abbreviation>
      </score-instrument>
      <midi-instrument id="Ibc39bf889c818704b55dea82a4df1357">
        <midi-channel>3</midi-channel>
        <midi-program>1</midi-program>
      </midi-instrument>
    </score-part>
  </part-list>
  <!--=========================== Part 1 ===========================-->
  <part id="P1">
    <!--========================= Measure 1 ==========================-->
    <measure implicit="no" number="1" width="310.24">
      <print>
        <system-layout>
          <system-margins>
            <left-margin>50</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <top-system-distance>195.14</top-system-distance>
        </system-layout>
      </print>
      <attributes>
        <divisions>10080</divisions>
        <key>
          <fifths>-4</fifths>
        </key>
        <time>
          <beats>3</beats>
          <beat-type>4</beat-type>
        </time>
        <clef>
          <sign>G</sign>
          <line>2</line>
        </clef>
      </attributes>
      <barline location="left">
        <repeat direction="forward" />
      </barline>
      <direction placement="above">
        <direction-type>
          <words default-y="39.46" enclosure="none" font-size="12" relative-y="10">Med. Swing</words>
        </direction-type>
      </direction>
      <direction placement="above">
        <direction-type>
          <words color="#808080" enclosure="none">est. global: d minor</words>
        </direction-type>
      </direction>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <direction placement="above">
        <direction-type>
          <metronome parentheses="no">
            <beat-unit>quarter</beat-unit>
            <per-minute>100</per-minute>
          </metronome>
        </direction-type>
        <sound tempo="100" />
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind text="m">minor</kind>
      </harmony>
      <note default-x="155.54" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note color="#FF0000" default-x="216.7" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>sharp</accidental>
        <stem>up</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
    </measure>
    <!--========================= Measure 2 ==========================-->
    <measure implicit="no" number="2" width="167.2">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
        <inversion>1</inversion>
        <bass>
          <bass-step>E</bass-step>
        </bass>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="73.66" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 3 ==========================-->
    <measure implicit="no" number="3" width="167.2">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>E</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="m6">minor-sixth</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="73.66" default-y="-15">
        <pitch>
          <step>C</step>
          <octave>5</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 4 ==========================-->
    <measure implicit="no" number="4" width="167.2">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>D</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="maj7">major-seventh</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note color="#FF0000" default-x="73.66" default-y="-10">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
    </measure>
    <!--========================= Measure 5 ==========================-->
    <measure implicit="no" number="5" width="167.2">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">B phrygian (1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>G</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="73.66" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>natural</accidental>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 6 ==========================-->
    <measure implicit="no" number="6" width="251.24">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <system-distance>176.89</system-distance>
        </system-layout>
      </print>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="107.07" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="164.02" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>flat</accidental>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 7 ==========================-->
    <measure implicit="no" number="7" width="156.67">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind text="m">minor</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note color="#FF0000" default-x="69.45" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>sharp</accidental>
        <stem>up</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
    </measure>
    <!--========================= Measure 8 ==========================-->
    <measure implicit="no" number="8" width="173.72">
      <barline location="left">
        <bar-style>regular</bar-style>
        <ending number="1" type="start" />
      </barline>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="69.45" default-y="-40">
        <pitch>
          <step>E</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
      <barline location="right">
        <ending number="1" type="stop" />
        <repeat direction="backward" />
      </barline>
    </measure>
    <!--========================= Measure 9 ==========================-->
    <measure implicit="no" number="9" width="196.74">
      <barline location="left">
        <bar-style>regular</bar-style>
        <ending number="2" type="start" />
      </barline>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="69.45" default-y="-40">
        <pitch>
          <step>E</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>15120</duration>
        <type>quarter</type>
        <dot />
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
      <note default-x="141.64" default-y="-30">
        <pitch>
          <step>G</step>
          <octave>4</octave>
        </pitch>
        <duration>2520</duration>
        <type>16th</type>
        <stem>up</stem>
        <beam number="1">begin</beam>
        <beam number="2">begin</beam>
      </note>
      <note color="#FF0000" default-x="166.95" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>2520</duration>
        <type>16th</type>
        <accidental>sharp</accidental>
        <stem>up</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
        <beam number="1">end</beam>
        <beam number="2">end</beam>
      </note>
      <barline location="right">
        <bar-style>regular</bar-style>
        <ending number="2" type="stop" />
      </barline>
    </measure>
    <!--========================= Measure 10 =========================-->
    <measure implicit="no" number="10" width="122.58">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind text="m">minor</kind>
      </harmony>
      <note default-x="12.5" default-y="-35">
        <pitch>
          <step>F</step>
          <octave>4</octave>
        </pitch>
        <duration>30240</duration>
        <tie type="start" />
        <type>half</type>
        <dot />
        <stem>up</stem>
        <notations>
          <tied type="start" />
        </notations>
      </note>
    </measure>
    <!--========================= Measure 11 =========================-->
    <measure implicit="no" number="11" width="128.08">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind text="m">minor</kind>
      </harmony>
      <note default-x="12.5" default-y="-35">
        <pitch>
          <step>F</step>
          <octave>4</octave>
        </pitch>
        <duration>30240</duration>
        <tie type="stop" />
        <type>half</type>
        <dot />
        <stem>up</stem>
        <notations>
          <tied type="stop" />
        </notations>
      </note>
      <barline location="right">
        <bar-style>light-light</bar-style>
      </barline>
    </measure>
    <!--========================= Measure 12 =========================-->
    <measure implicit="no" number="12" width="383.27">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <system-distance>176.89</system-distance>
        </system-layout>
      </print>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>D</root-step>
        </root>
        <kind>half-diminished</kind>
      </harmony>
      <note default-x="107.41" default-y="-45">
        <pitch>
          <step>D</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>15120</duration>
        <type>quarter</type>
        <dot />
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
      <note default-x="213.73" default-y="-35">
        <pitch>
          <step>F</step>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
      </note>
      <note color="#FF0000" default-x="269.64" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
        <beam number="1">begin</beam>
      </note>
      <note default-x="325.55" default-y="-15">
        <pitch>
          <step>C</step>
          <octave>5</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>down</stem>
        <beam number="1">end</beam>
      </note>
    </measure>
    <!--========================= Measure 13 =========================-->
    <measure implicit="no" number="13" width="178.12">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>G</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="16.84" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>30240</duration>
        <type>half</type>
        <dot />
        <accidental>natural</accidental>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 14 =========================-->
    <measure implicit="no" number="14" width="293.87">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">a minor (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
      </note>
      <note default-x="68.41" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <accidental>flat</accidental>
        <stem>down</stem>
      </note>
      <note color="#FF0000" default-x="152.28" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <accidental>sharp</accidental>
        <stem>up</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
      <note default-x="236.15" default-y="-25">
        <pitch>
          <step>A</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <tie type="start" />
        <type>eighth</type>
        <accidental>natural</accidental>
        <stem>up</stem>
        <notations>
          <tied type="start" />
        </notations>
      </note>
    </measure>
    <!--========================= Measure 15 =========================-->
    <measure implicit="no" number="15" width="173.78">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-25">
        <pitch>
          <step>A</step>
          <octave>4</octave>
        </pitch>
        <duration>30240</duration>
        <tie type="stop" />
        <type>half</type>
        <dot />
        <stem>up</stem>
        <notations>
          <tied type="stop" />
        </notations>
      </note>
    </measure>
    <!--========================= Measure 16 =========================-->
    <measure implicit="no" number="16" width="347.91">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <system-distance>176.89</system-distance>
        </system-layout>
      </print>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>B</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="m7">minor-seventh</kind>
      </harmony>
      <note default-x="107.07" default-y="-55">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>3</octave>
        </pitch>
        <duration>15120</duration>
        <type>quarter</type>
        <dot />
        <stem>up</stem>
      </note>
      <note default-x="199.8" default-y="-35">
        <pitch>
          <step>F</step>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
      </note>
      <note default-x="248.57" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
        <beam number="1">begin</beam>
      </note>
      <note color="#008000" default-x="297.34" default-y="-25">
        <pitch>
          <step>A</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <accidental>flat</accidental>
        <stem>up</stem>
        <notehead color="#008000" parentheses="no">normal</notehead>
        <beam number="1">end</beam>
      </note>
    </measure>
    <!--========================= Measure 17 =========================-->
    <measure implicit="no" number="17" width="153.4">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>E</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-30">
        <pitch>
          <step>G</step>
          <octave>4</octave>
        </pitch>
        <duration>30240</duration>
        <type>half</type>
        <dot />
        <stem>up</stem>
      </note>
    </measure>
    <!--========================= Measure 18 =========================-->
    <measure implicit="no" number="18" width="258.14">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">g minor (-2)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>A</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind>major</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
      </note>
      <note default-x="61.27" default-y="-40">
        <pitch>
          <step>E</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="134.42" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>down</stem>
      </note>
      <note color="#FF0000" default-x="207.57" default-y="-25">
        <pitch>
          <step>G</step>
          <alter>1</alter>
          <octave>4</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <accidental>sharp</accidental>
        <stem>up</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
    </measure>
    <!--========================= Measure 19 =========================-->
    <measure implicit="no" number="19" width="269.58">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">d minor (-1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>G</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="18.44" default-y="-55">
        <pitch>
          <step>B</step>
          <alter>0</alter>
          <octave>3</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
      <note default-x="67.21" default-y="-45">
        <pitch>
          <step>D</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="140.36" default-y="-30">
        <pitch>
          <step>G</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="213.51" default-y="-55">
        <pitch>
          <step>B</step>
          <octave>3</octave>
        </pitch>
        <duration>5040</duration>
        <type>eighth</type>
        <stem>up</stem>
      </note>
      <barline location="right">
        <bar-style>light-light</bar-style>
      </barline>
    </measure>
    <!--========================= Measure 20 =========================-->
    <measure implicit="no" number="20" width="328.18">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <system-distance>176.89</system-distance>
        </system-layout>
      </print>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind>major</kind>
      </harmony>
      <note default-x="107.07" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="194.8" default-y="-25">
        <pitch>
          <step>A</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
    </measure>
    <!--========================= Measure 21 =========================-->
    <measure implicit="no" number="21" width="233.62">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
        <inversion>1</inversion>
        <bass>
          <bass-step>E</bass-step>
        </bass>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="100.23" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 22 =========================-->
    <measure implicit="no" number="22" width="233.62">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>E</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="m6">minor-sixth</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="100.23" default-y="-15">
        <pitch>
          <step>C</step>
          <octave>5</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 23 =========================-->
    <measure implicit="no" number="23" width="233.62">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>D</root-step>
          <root-alter>-1</root-alter>
        </root>
        <kind text="maj7">major-seventh</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note color="#FF0000" default-x="100.23" default-y="-10">
        <pitch>
          <step>C</step>
          <alter>1</alter>
          <octave>5</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>sharp</accidental>
        <stem>down</stem>
        <notehead color="#FF0000" parentheses="no">normal</notehead>
      </note>
    </measure>
    <!--========================= Measure 24 =========================-->
    <measure implicit="no" number="24" width="325.88">
      <print new-system="yes">
        <system-layout>
          <system-margins>
            <left-margin>0</left-margin>
            <right-margin>0</right-margin>
          </system-margins>
          <system-distance>176.89</system-distance>
        </system-layout>
      </print>
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">B phrygian (1)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>G</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="107.07" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="193.88" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>natural</accidental>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 25 =========================-->
    <measure implicit="no" number="25" width="231.32">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>C</root-step>
        </root>
        <kind text="7">dominant</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="99.31" default-y="-20">
        <pitch>
          <step>B</step>
          <alter>-1</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>flat</accidental>
        <stem>down</stem>
      </note>
    </measure>
    <!--========================= Measure 26 =========================-->
    <measure implicit="no" number="26" width="231.32">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind>major</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="99.31" default-y="-25">
        <pitch>
          <step>A</step>
          <alter>0</alter>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <accidental>natural</accidental>
        <stem>up</stem>
      </note>
    </measure>
    <!--========================= Measure 27 =========================-->
    <measure implicit="no" number="27" width="240.52">
      <direction>
        <direction-type>
          <words color="#808080" enclosure="none" font-style="italic">E phrygian (0)</words>
        </direction-type>
      </direction>
      <harmony>
        <root>
          <root-step>F</root-step>
        </root>
        <kind>major</kind>
      </harmony>
      <note default-x="12.5" default-y="-50">
        <pitch>
          <step>C</step>
          <octave>4</octave>
        </pitch>
        <duration>10080</duration>
        <type>quarter</type>
        <stem>up</stem>
      </note>
      <note default-x="99.31" default-y="-35">
        <pitch>
          <step>F</step>
          <octave>4</octave>
        </pitch>
        <duration>20160</duration>
        <type>half</type>
        <stem>up</stem>
      </note>
      <barline location="right">
        <bar-style>light-heavy</bar-style>
      </barline>
    </measure>
  </part>
</score-partwise>