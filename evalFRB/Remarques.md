# Remarques



- [x] nombre d'erreurs (diff)

incohérences entre table et marquage score

e.g. `Black Nile`, `Corcovado`: 0 marques



- [ ] `PSE_30_2_A_MD_AD+_TD_20250117-1627` `Black Orpheus`

mesure 375 et d'autres mesures la min

Ab pour G# alors que A mineur était bien indentifié

calcul table T2 avec AD+ ??



- [ ] `PSE_135_2_A_MD_AD+_TD_20250122-1101`    `Equinox` 

```
rewrite passing notes
[error] Speller ilocal: no bar 16
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 17
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 18
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 19
[error] Ton constructor: UNDEF mode
```



- [ ] `PSE_135_2_A_MD_AD+_TD_20250122-1101`    `Dindi` 

```
rewrite passing notes
[error] Speller ilocal: no bar 32
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 33
[error] Ton constructor: UNDEF mode
```



- [ ] `PSE_135_2_A_MD_AD+_TD_20250122-1101`    `Bud's blues` 

```
rewrite passing notes
[error] Speller ilocal: no bar 10
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 11
[error] Ton constructor: UNDEF mode
[error] Speller ilocal: no bar 12
[error] Ton constructor: UNDEF mode
```

